Based on https://www.iviewui.com
Github: https://github.com/iview/iview

Laravel project with iview:
https://gitlab.com/connecteev/laravel_iview/

Installation Steps:

To install and run this, do a git clone: git@gitlab.com:connecteev/laravel_iview.git
Then run:
composer install
cp .env.example .env
php artisan key:generate
npm install
Terminal 1: npm run watch
Terminal 2: php artisan serve --port=8000
Then go to http://127.0.0.1:8000/

Note:
My code is in resources/views/welcome.blade.php and resources/js/app.js (which gets built into public/js/app.js)

Note:
------
Reached out to sergiosbox@gmail.com about this problem:
Why do the styles not match the demos for https://www.iviewui.com/components/progress-en and https://www.iviewui.com/components/select-en?
I am not using the cdn, but I have imported the css: 
import 'iview/dist/styles/iview.css';
